using namespace System.Net

# Input bindings are passed in via param block.
param($Request, $TriggerMetadata)

# Write to the Azure Functions log stream.
Write-Host "PowerShell HTTP trigger function processed a request."

$rssUri =  'https://www.bibeln.se/pren/syndikering.jsp'
function CleanHTML {
    param ([string]$html)
    $html = $html -replace "<[^>]+>", "" # html tags
    $html = $html.replace("`n"," ").replace("`r"," ") # line breaks
    $html = $html -replace '\s+', ' ' # multiple consecutive spaces
    $html = $html.replace("&#8211;","—").replace("&#8221;","””") # line breaks
    $html = $html.replace("&#8216;","””").replace("&#8217;","””") # actually single quotes, but that's just wrong
    $html = $html.replace("&#8230;","...") # mac triple dot
    $html = $html.replace("&amp;","&") # ampersand
    $html = $html.replace("&nbsp;"," ") # space
    $html = $html.Trim() # leading and trailing spaces

    return $html
}

$rssResponse    = Invoke-WebRequest -UseBasicParsing -Method Get -Headers $defaultHeaders -Uri $rssUri
$quote          = $rssResponse.Content
$quote          = $quote.Substring(110)                                                 # trim leading characters
$quoteText      = $quote.Substring(0, $quote.IndexOf('<a class="bibel_link"'))          # extract quote
$quote          = $quote.Substring($quote.IndexOf('<a class="bibel_link"'))             # trim quote
$quote          = $quote.TrimStart('<a class="bibel_link" href="')                      # trim HTML
$quote          = $quote.Substring($quote.IndexOf("""") + 2)                            # trim HTML
$title          = $quote.Substring(0, $quote.IndexOf('<'))                              # extract verse
$pubDate        = (Get-Date).ToString('yyyy-MM-dd HH:mm:ss')
# HTML decode
Write-Host "Citat: $quoteText ($title)"
Add-Type -AssemblyName System.Web
$quote = [System.Web.HttpUtility]::HtmlDecode($quoteText)
Write-Host "HTML-kodat citat: $quote"
$cleanQuote = (CleanHTML $quote)
Write-Host "Rensat citat: $cleanQuote"
$returnArray = @{
	theVerse       = $title
	theQuote       = $cleanQuote
       theDate        = $pubDate
}
$body = $returnArray | ConvertTo-Json

Write-Host "JSON-Body: $body"

# Associate values to output bindings by calling 'Push-OutputBinding'.
Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
    StatusCode = [HttpStatusCode]::OK
    Body = $body
})
